package homework.lesson09;

public class RequestClass {
    public static void main(String[] args) {
        RealLife r = new RealLife();
        r.age();
        String cityName = r.cityName("Orhei");
        System.out.println(cityName);
        r.showMessage();
        r.street("Decebal", 5);
    }
}
