package homework.lesson10.p1.phoneParametres;

import homework.lesson10.p1.phone.Phone;

public class PhoneParametres {

//De ce nu pot apela parametrii in afara clasei main? - Se poate apela in metoda.
    public static void main(String[] args) {
    Phone p = new Phone();
    p.internet4g = true;
    p.manufactored = "China";
    p.model = "Nokia";

    }
}
